## Fonctionnalités :
 1) stocker des livres
 2) les restituer
 - par ordre alphabétique de titre
 - par ordre alphabétique d'auteurs
 3) faire des recherches
 - sur le titre
 - sur l'auteur
2) Dans votre dossier de projet (local): initialiser un dépôt public Git
3) Y créer un fichier README.md et y ajouter du texte (à personnaliser, évidemment !